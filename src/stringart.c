#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <err.h>
#include <argp.h>
#include <unistd.h>
#include <stdlib.h>

#define ARGS_DOC "STRING"
#define DOC "swastika -- swastika is a program which outputs a string in a swastika pattern!"
#define ARGP_PROGRAM_BUG_ADDRESS "<doche_j@epita.fr>"

#define OPT_CONFLICT "Conflicting options"
#define SWASTIKA_INPUT_FILE ""
#define BUFF_SIZE 1000

static struct argp_option options[] =
{
  {"input", 'i', "input_file", 0, "input file", 0},
  {"output", 'o', "output_file", 0, "output file", 0},
  {"swastika", 333, NULL, OPTION_HIDDEN | OPTION_ARG_OPTIONAL, "swastika pattern", 0},
  {"synergie", 444, NULL, OPTION_ARG_OPTIONAL, "Enable certified synergie complient output", 0},
  {"facebook", 555, NULL, OPTION_ARG_OPTIONAL,
   "output in a format compatible with Facebook", 0},
  {"vegan", 666, NULL, OPTION_ARG_OPTIONAL, "Vegan flavoured output", 0},
  {0}
};

enum flags
{
  OPT_DONE = 1,
  OPT_SWASTIKA = 2,
  OPT_INPUT = 4,
  OPT_SYNERGIE = 8,
  OPT_FB = 16,
  OPT_VEGAN = 32,
};

struct args
{
  char *input;
  char *output;
  char *string;
  enum flags flags;
};

static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
  struct args *args = state->input;
  switch (key)
  {
    case 'i':
      if (args->flags & OPT_SWASTIKA)
        errx(1, "%s --input and --swastika", OPT_CONFLICT);
      args->flags |= OPT_INPUT;
      args->input = arg;
      break;
    case 'o':
      args->output = arg;
      break;
    case 333:
      if (args->flags & OPT_INPUT)
        errx(1, "%s --input and --swastika", OPT_CONFLICT);
      args->flags |= OPT_SWASTIKA;
      args->input = SWASTIKA_INPUT_FILE;
      break;
    case 444:
      if (args->flags & OPT_VEGAN)
        errx(1, "%s --vegan and --synergie", OPT_CONFLICT);
      args->flags |= OPT_SYNERGIE;
      break;
    case 555:
      args->flags |= OPT_FB;
      break;
    case 666:
      if (args->flags & OPT_SYNERGIE)
        errx(1, "%s --vegan and --synergie", OPT_CONFLICT);
      args->flags |= OPT_VEGAN;
      break;
    case ARGP_KEY_ARG:
      if (state->arg_num >= 1)
        argp_usage(state);
      args->string = arg;
      break;
    case ARGP_KEY_END:
      if (!(args->flags & OPT_SWASTIKA) && !(args->flags & OPT_INPUT))
        errx(1, "No input pattern specified");
      break;
    default:
      return ARGP_ERR_UNKNOWN;
  }
  return 0;
}

int print_swastika(int fd, char *string, int letter_sp_rat)
{
  int len = strlen(string);
  if (len < 3)
    errx(1, "minimum 3 char in string");
  for (int i = len - 1; i >= 0; i--)
  {
    dprintf(fd, "%c", string[i]);
    if (i == len - 1)
    {
      dprintf(fd, "%*c", (1 + letter_sp_rat) * (len - 1) - 1, ' ');
      for (int j = 0; j < len; j++)
        dprintf(fd, "%c%*c", string[j], (j != len - 1) ? 1 : 0, ' ');
    }
    else if (i == 0)
    {
      for (int j = 1; j < len; j++)
        dprintf(fd, " %c", string[j]);
      for (int j = len - 2; j >= 0; j--)
        dprintf(fd, " %c", string[j]);
    }
    else
      dprintf(fd, "%*c%c",
              (1 + letter_sp_rat) * (len - 1) - 1, ' ',
              string[len - i - 1]);
    dprintf(fd, "\n");
  }
  for (int i = 1; i < len; i++)
  {
    if (i == len - 1)
    {
      for (int j = len - 1; j >= 0; j--)
        dprintf(fd, "%c ", string[j]);
      dprintf(fd, "%*c%c",
              (1 + letter_sp_rat) * (len - 1) - 3 + letter_sp_rat, ' ',
              string[i]);
    }
    else
      dprintf(fd, "%*c%c%*c%c",
              (1 + letter_sp_rat) * (len) + letter_sp_rat - 3, ' ',
              string[len - i - 1],
              (1 + letter_sp_rat) * (len - 1) + 2 * letter_sp_rat - 3, ' ',
              string[i]);
    dprintf(fd, "\n");
  }
  return 0;
}

struct buff
{
  char *data;
  int index;
  int len;
} g_buff =
{
  0
};

static void fill_buff()
{
  g_buff.data = malloc(sizeof (char) * BUFF_SIZE);
  g_buff.len = read(0, g_buff.data, BUFF_SIZE);
  g_buff.index = 0;
  if (g_buff.len < 0)
    err(errno, "error");
}

int get_char()
{
  if (g_buff.index >= g_buff.len)
    fill_buff();
  return (g_buff.index >= g_buff.len) ? -1 : g_buff.data[g_buff.index++];
}

int main(int argc, char **argv)
{
  struct args args =
  {
    0
  };
  struct argp argp = {options, parse_opt, ARGS_DOC, DOC, 0, 0, 0};
  argp_parse(&argp, argc, argv, 0, 0, &args);
  int fd = 0;
  int ret = 0;
  int readded = 0;
  size_t size = 0;
  int letter_sp_rat = ((args.flags & OPT_FB) ? 1 : 0) + 1;
  char *str = args.string;
  if (args.output)
  {
    fd = open(args.output, O_CREAT | O_RDWR);
    if (fd < 0)
      err(errno, "Cannot open %s", args.output);
  }
  FILE *f = fdopen(0, "r");
  if (!f)
    err(1, "error");
  do {
    if (!args.string)
      readded = getline(&str, &size, f);
    if (readded == -1)
      err(1, "error");
    if (readded > 1)
      str[readded - 1] = 0;
    if (args.flags & OPT_INPUT)
      errx(1, "Not implemented yet");
    if (args.flags & OPT_SYNERGIE)
      dprintf(fd, "\e[95m");
    if (args.flags & OPT_VEGAN)
      dprintf(fd, "\e[92m");
    if (args.flags & OPT_SWASTIKA)
      ret = print_swastika(fd, str, letter_sp_rat);
  } while (!(args.string) && readded != -1);
  close(fd);
  return ret;
}
